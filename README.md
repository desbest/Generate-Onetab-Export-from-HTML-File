# Generate Onetab Export from HTML File

This was made for a specific rare use case I had for when using System Restore corrupted my data for a firefox extension. So I had to find a clever way to backup my data by saving what was visible as an HTML file, then using my script I did the programming for, to then  export it in a [onetab](https://www.one-tab.com/) compatible format, so I could finally import it again on a fresh onetab installation.

## How To Install
You will need
* PHP 5.60 and above. It's also PHP 7 compatible.
* iconv php extension
* curl php extension
* mbstring php extension
* allow_fsock_open to be on in php.ini (for if curl isn't available)

You can check if you have these installed by creating a new php file that has `<?php phpinfo(); ?>`

Simply upload it to a folder on a website (or your localhost)

## How To Use
1. Extract all the files to a folder on your website
2. Have a file titled `scraped.html` that folder which is the HTML file of where you saved the web browser output of your onetab page
3. Open the folder in your web browser and you'll see a onetab compatible import/export format in the large text box (or `<textarea>`).

If you don't have a website you can use Uniform Server to run it off your own computer by using [Uniform Server](http://uniformserver.com/) or [Laragon](https://laragon.org/) if you use windows. I recommend Uniform Server.

## Credits
* Programming by [desbest](http://desbest.com) [[gitlab]](http://gitlab.com/desbest) 
* 17/07/2021
* MIT License
