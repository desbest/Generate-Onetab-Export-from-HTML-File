<?php
/*  
    Generate Onetab Export from HTML File
    Written by desbest
    MIT License
    https://gitlab.com/desbest/Generate-Onetab-Export-from-HTML-File
*/
// // exit("last try");

function formatBytes($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 

// look for the string "21992199219" in text.html
// adding an extra character at the end like "21992194" makes the scraper fail

// setup
error_reporting(E_ALL);
ini_set('display_errors', '1');
$allocatenumber = (20 * 10)."M"; // allow an extra 18MB for processing
// $timelimitnumber = str_replace("M", "", $allocatenumber) / 10000;
ini_set( 'memory_limit', $allocatenumber );
// also check constants.php in simplehtmldom-2rc2/

// set_time_limit($timelimitnumber);
// $timelimithuman = $timelimitnumber / 60;

// ini_set( 'max_input_time', 5000 );
// ini_set( 'max_execution_time', 5000 );
// ini_set( 'max_input_vars', 5000 );
// ini_set( 'max_input_nesting_level', 5000 );

echo "$allocatenumber"."B memory is allocated<hr>";
// echo "the time limit seconds are $timelimitnumber seconds which is $timelimithuman minutes<br>";

// setup is finished


// This example illustrates how to use basic selectors to retrieve HTML contents
require_once 'simplehtmldom-2rc2/HtmlWeb.php';
use simplehtmldom\HtmlWeb;
// exit("hello");

// get DOM from URL or file
$doc = new HtmlWeb();
$html = $doc->load('http://localhost/onetab/scrapedtidy.html');

//memory check
$mymemoryusage = memory_get_usage();
$humanmemoryusage = formatBytes($mymemoryusage);
echo "my memory usage is $mymemoryusage bytes<br>"; 
echo "my human readable usage is $humanmemoryusage <hr>"; 
//end memory check

if (isset($html)){ echo "scraped successfully<hr>"; }
    else { echo "FAILED TO SCRAPE<hr>";}
//var_dump($html);

//////////
// PART 1: See my onetab compatible export

// find all div tags with class="tabGroup"
echo "<h1>Below is your export file in onetab format</h1>";
echo "<textarea style=\"width: 700px; height: 420px;\">";
foreach($html->find('div.tabGroup') as $tabgroup){

     foreach ($tabgroup->find('.tabLink') as $tablink){
            echo $tablink->href." | ".$tablink->innertext;
            // echo '<br>';
            echo PHP_EOL;
    }
    // echo '<br>';
    echo PHP_EOL;
    // var_dump($tabgroupname);
    // echo $tabgroupname->innertext . '<br>' . PHP_EOL;
}
echo "</textarea>";

//////////
// PART 2: See the names of all my named (not all) tab groups 

echo "<hr>
<h1>Below are all your named tab groups</h1>";
echo "<ol>";
foreach($html->find('div.tabGroup') as $tabgroup2){

foreach ($tabgroup2->find('.tabGroupTitleText') as $tabgroupname2){
    $sanitised = str_replace("\u{00a0}", null, $tabgroupname2->innertext);
    // https://stackoverflow.com/a/59505744/337306
    // unicode control character
    if (!empty($sanitised)){ echo "<li>$sanitised</li>"; }
}
}
echo "</ul>";
//exit(); //debug

//////////
// PART 3: See the links in each group

foreach($html->find('div.tabGroup') as $tabgroup3){
foreach ($tabgroup3->find('.tabGroupTitleText') as $tabgroupname3){
    //echo json_encode(($tabgroupname->innertext)). "<br>"; 
    //debug --> find out what the control character type "space" is

    $sanitised = str_replace("\u{00a0}", null, $tabgroupname3->innertext);
    // https://stackoverflow.com/a/59505744/337306
    // unicode control character

    if (!empty($sanitised)){
        echo $sanitised."<br>";
        //echo PHP_EOL;
    }

    if (!empty($sanitised)){
        echo "<br><textarea style=\"width: 700px; height: 420px;\">";
        foreach ($tabgroup3->find('.tabLink') as $tablink){
            echo $tablink->href." | ".$tablink->innertext;
            // echo '<br>';
            echo PHP_EOL;
        }
        echo "</textarea><br><br><br>";
    }
}
}